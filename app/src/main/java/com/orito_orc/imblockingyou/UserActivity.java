package com.orito_orc.imblockingyou;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.InputType;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class UserActivity extends AppCompatActivity
{
    Button btn_blocking;
    Button check_blocked;
    BroadcastReceiver br;
    ImageView image;
    final int REQUEST_IMAGE_CAPTURE = 1;
    String plate;
    String phone;
    Boolean blocking = false;
    LinearLayout linear;
    TextView info;
    final String not_blocking = "You are not blocking anyone";

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);
        linear = findViewById(R.id.linear_id);
        linear.getBackground().setAlpha(220);
        info = (TextView)findViewById(R.id.textView);
        check_blocked = (Button)findViewById(R.id.check_blocked);

        info.setText(not_blocking);
        btn_blocking = (Button) findViewById(R.id.blocking_btn);
        final String block_text = "BLOCK A CAR";
        btn_blocking.setText(block_text);
//        image = (ImageView) findViewById(R.id.image_taken);
        plate = getIntent().getStringExtra("plate");
        check_blocked.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), Blocked.class);
                i.putExtra("plate", plate);
                startActivity(i);

            }

        });
        br = new MyBroadcastReceiver(){
            @Override
            public void onReceive(Context context, Intent intent) {
                Log.d("Debug", "User On Receive");
                Boolean am_i_blocking = intent.getBooleanExtra("am_i_blocking", false);
                String blocked_plate = intent.getStringExtra("blocked_plate");
                if(!am_i_blocking || blocked_plate == null){
                    blocking = false;
                    btn_blocking.setText(block_text);
                    return;
                }
                String text = "You Are Blocking \n\n" + plate;
                String block_txt = "Stop Blocking";
                btn_blocking.setText(block_txt);
                info.setText(text);
                blocking = true;


            }
        };
        IntentFilter intentFilter = new IntentFilter("am_i_blocking");
        this.registerReceiver(br,intentFilter);
        am_i_blocking(plate);
        btn_blocking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!blocking)
                {
                    block(v);

                }
                else
                {
                    stop_blocking(v);
                    btn_blocking.setText(block_text);
                    info.setText(not_blocking);
                    blocking = false;
                }
            }
        });
    }


    @Override
    protected void onPause()
    {
        super.onPause();
        try {
            if(br != null){
                unregisterReceiver(br);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    protected void onResume()
    {
        super.onResume();
        IntentFilter intent = new IntentFilter("am_i_blocking");
        this.registerReceiver(br, intent);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id == R.id.logout){
            SharedPreferences sharedPref = this.getSharedPreferences("user_data", Context.MODE_PRIVATE);
            SharedPreferences.Editor edit = sharedPref.edit();
            edit.remove("plate");
            edit.apply();
            Intent i = new Intent(this, MainActivity.class);
            startActivity(i);
        }
        else if(id == R.id.exit){
            finishAndRemoveTask();
        }
        else if(id == R.id.about){
            AlertDialog.Builder alert = new AlertDialog.Builder(this);
            alert.setTitle("About");
            alert.setMessage("ImBlockingYou App\nDeveloped by Or cohen and Ori Tor\nAndroid studio final project.");
            alert.setPositiveButton("Ok", null);
            alert.show();
        }
        return super.onOptionsItemSelected(item);
    }
    public AlertDialog.Builder makeAlertDialog()
    {
        AlertDialog.Builder alert =  new AlertDialog.Builder(this);
        alert.setTitle("Who are you blocking ?");
        alert.setMessage("Insert the plate number of the car you are blocking");
        final EditText message = new EditText(this);
        message.setInputType(InputType.TYPE_CLASS_NUMBER);
        message.setHint("Enter plate number");
        alert.setView(message);
        // Specifying a listener allows you to take an action before dismissing the dialog.
        // The dialog is automatically dismissed when a dialog button is clicked.
        alert.setPositiveButton("Search", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                final String blocking_plate = message.getText().toString();
                final FirebaseFirestore db = FirebaseFirestore.getInstance();
                db.collection("users").document(blocking_plate).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                        if(task.isSuccessful()){
                            DocumentSnapshot res = task.getResult();
                            if(res.exists()){
                                Log.d("Debug", "--------"+res.getData());
                                Map<String, Object> data = new HashMap<>(res.getData());
                                Log.d("Debug", blocking_plate);
                                Map<String, Object> blocker_data = new HashMap<>();
                                blocker_data.put("blocker", plate);
                                final SharedPreferences sharedPref = getApplicationContext().getSharedPreferences("user_data", Context.MODE_PRIVATE);
                                String b_phone = sharedPref.getString("phone", "No Phone");
                                blocker_data.put("phone", b_phone);
                                Intent service = new Intent(getApplicationContext(), MyFGService.class);
                                service.putExtra("flags", 3); // indicates block_user invocation
                                service.putExtra("plate", blocking_plate);
                                service.putExtra("hashMap", (Serializable)blocker_data);
                                startForegroundService(service);
                                String block_txt = "Click to stop blocking";
                                btn_blocking.setText(block_txt);
                                String text = "You Are Blocking \n\n" + blocking_plate;
                                btn_blocking.setText(block_txt);
                                info.setText(text);
                                blocking = true;

                            }
                            else{
                                Toast.makeText(getApplicationContext(), "Cannot find plate number", Toast.LENGTH_LONG).show();
                            }
                        }
                    }
                });
            }
        });
        // A null listener allows the button to dismiss the dialog and take no further action.
        alert.setNegativeButton("Cancel", null);
        alert.setIcon(android.R.drawable.ic_dialog_alert);
        return alert;
    }
    public void block(View v)
    {
        AlertDialog.Builder alert = makeAlertDialog();
        alert.show();
    }
    public void stop_blocking(View v){
        Intent i = new Intent(this, MyFGService.class);
        i.putExtra("flags", 5); // indicates stop_blocking
        i.putExtra("plate", plate);
        Log.d("Debug", "-----fddgfdgdfg----");

        startForegroundService(i);
    }

    public void takePic(){
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            Bitmap imageBitmap = (Bitmap) extras.get("data");
            image.setImageBitmap(imageBitmap);
        }
    }
    public void am_i_blocking(String plate){
        Intent i = new Intent(this, MyFGService.class);
        i.putExtra("flags", 6); // indicates check if im_blocking
        i.putExtra("plate", plate);
        Log.d("Debug", "-----1----");

        startForegroundService(i);
    }

}
