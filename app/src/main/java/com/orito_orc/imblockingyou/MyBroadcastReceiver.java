package com.orito_orc.imblockingyou;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;



public class MyBroadcastReceiver extends BroadcastReceiver
{
    private static final String TAG = "MyBroadcastReceiver";
    @Override
    public void onReceive(Context context, Intent intent)
    {
        //Extract your data - better to use constants...
        String blocker=intent.getStringExtra("test");
        if(blocker.isEmpty()){
            Log.d("Debug", "Not the broadcase we wanted");
        }
        else {
            Log.d("Debug", "----On Receive----");
            Log.d("Debug", "----" + blocker + "----");
            Intent i = new Intent("android.intent.action.MAIN");
            i.putExtra("test", blocker);
            context.sendBroadcast(i);

        }

    }
}
