package com.orito_orc.imblockingyou;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.firestore.FirebaseFirestore;
import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {
    EditText password;
    EditText plate;
    EditText phone_number;
    Button sign_in;
    Button sign_up;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        password = (EditText)findViewById(R.id.password);
        phone_number = (EditText)findViewById(R.id.phone_number);
        sign_in = (Button)findViewById(R.id.sign_in);
        plate = (EditText)findViewById(R.id.plate_num);
        sign_up = (Button) findViewById(R.id.sign_up);
        final SharedPreferences sharedPref = this.getSharedPreferences("user_data", Context.MODE_PRIVATE);
        final String sp_plate = sharedPref.getString("plate", null);
        Log.d("Debug", "plate from sp "+sp_plate);
        if(sp_plate != null)
        {
            Intent i = new Intent(getApplicationContext(), UserActivity.class);
            i.putExtra("plate", sp_plate);
            startActivity(i);
        }

    }

    public void sign_in(View v)
    {
        final String plate = this.plate.getText().toString();
        final String pass = this.password.getText().toString();
        if(plate.isEmpty() || pass.isEmpty()) {
            Toast.makeText(this, "plate/password invalid", Toast.LENGTH_LONG).show();
            return;
        }
        // todo : call the sign-in method fom within fgservice
        Intent service = new Intent(getApplicationContext(), MyFGService.class);

        service.putExtra("flags", 2); // indicates to call sign-in method
        service.putExtra("plate", plate);
        service.putExtra("pass", pass);
        startForegroundService(service);


    }

    public void sign_up(View v){
        String password = this.password.getText().toString();
        String plate = this.plate.getText().toString();
        String phone = this.phone_number.getText().toString();
        if(password.isEmpty() || plate.isEmpty() || phone.isEmpty()) {
            Toast.makeText(this, "plate/password/phone invalid", Toast.LENGTH_LONG).show();
            Log.d("Debug", "plate "+plate);
            return;
        }
        final SharedPreferences sharedPref = getApplicationContext().getSharedPreferences("user_data", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString("plate", plate);
        editor.putString("phone", phone);
        editor.apply();
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        Map<String, Object> data = new HashMap<>();
        data.put("plate", plate);
        data.put("password", password);
        data.put("phone_number", phone);
        db.collection("users").document(plate).set(data);

        Intent i = new Intent(this, UserActivity.class);
        i.putExtra("plate", plate);
        startActivity(i);
    }
}
