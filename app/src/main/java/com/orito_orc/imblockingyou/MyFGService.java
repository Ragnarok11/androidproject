package com.orito_orc.imblockingyou;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.widget.EditText;


import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;


import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.HashMap;
import java.util.Map;

public class MyFGService extends Service {
    private static final String NOTIFICATION_CHANNEL_ID = "IM_BLOCKING_YOU";
    public static final String CHANNEL_ID = "FGServiceChannel";
    public static final int FG_NOTIFICATION_ID = 111;

    private Intent MyIntent;

    private NotificationManager notificationManager;
    private NotificationChannel notificationChannel;

    @Override
    public int onStartCommand(final Intent intent, final int flags, int startId) {
//        createNotificationChannel();
        Log.d("Debug", "MyFGService - onStartCommand()");
        MyIntent = intent;
        createFGNotificationRunning();

        // do heavy work on a background thread
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(100);
                    int flag = intent.getIntExtra("flags", -1);
                    if (flag == 2){
                    String plate = intent.getStringExtra("plate");
                    String pass = intent.getStringExtra("pass");
                    signUserIn(plate, pass);
                    }
                    else if(flag == 3){
                        Log.d("Debug", "_____INSIDE BLOCK_USER____");
                        String plate = intent.getStringExtra("plate");
                        Map<String, Object> blocker_data = (Map<String, Object>)intent.getSerializableExtra("hashMap");
                        block_user(plate, blocker_data);
                    }
                    else if(flag == 4)
                    {
                        final Intent i = new Intent();
                        Thread.sleep(1000);
                        String plate = intent.getStringExtra("plate");
                        search_blockers(plate);
                        i.putExtra("test", "test");
                        i.setAction("android.intent.action.MAIN");
                        sendBroadcast(i);
                    }
                    else if(flag == 5){
                        String plate = intent.getStringExtra("plate");
                        stop_blocking(plate);
                    }
                    else if(flag == 6){
                        String plate = intent.getStringExtra("plate");
                        am_i_blocking(plate);
                    }
                    createFGNotificationFinish();
                    stopMyForegroundService();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();

        return START_NOT_STICKY;
    }

    private void createFGNotificationFinish() {
        // Create a notification manager object.
        notificationManager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);

        // Notification channels are only available in OREO and higher.
        // So, add a check on SDK version.
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            // Create the NotificationChannel with all the parameters.
            notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, NOTIFICATION_CHANNEL_ID, NotificationManager.IMPORTANCE_HIGH);
            notificationChannel.setDescription(NOTIFICATION_CHANNEL_ID);
            notificationManager.createNotificationChannel(notificationChannel);
        }


        Notification notification = new NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID)
                .setContentTitle("App is updated!")
                .setContentText("Log in to check updated data")
                .setSmallIcon(R.drawable.ic_launcher_background)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setAutoCancel(true)
                .build();

        // Deliver the notification
        notificationManager.notify(0, notification);

    }

    private void createFGNotificationRunning() {
        // create Notification Channel
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel serviceChannel = new NotificationChannel(
                    CHANNEL_ID,
                    "FG Service Channel",
                    NotificationManager.IMPORTANCE_HIGH
            );
            NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            manager.createNotificationChannel(serviceChannel);
        }

        Intent notificationIntent = new Intent(this, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);

        // create Notification
        Notification notification = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setContentTitle("Updating...")
                .setContentText("Tap to Stop this FG Service!")
                .setSmallIcon(R.drawable.common_google_signin_btn_icon_dark)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                //.setCategory(NotificationCompat.CATEGORY_MESSAGE)
                .setContentIntent(pendingIntent)
                .build();

        startForeground(FG_NOTIFICATION_ID, notification);
    }


    @Override
    public void onDestroy() {
        super.onDestroy();

        Log.d("debug", "MyFGService.onDestroy()");
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    public void startMyForegroundService() {
        Intent serviceIntent = new Intent(this, MyFGService.class);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            Log.d("debug", "FG Service Started!");
            startForegroundService(serviceIntent);
        }
    }

    public void stopMyForegroundService() {
        Log.d("debug", "FG Service Stoped!");
        stopService(MyIntent);

    }

    public void signUserIn(final String plate, final String pass) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        final SharedPreferences sharedPref = getApplicationContext().getSharedPreferences("user_data", Context.MODE_PRIVATE);
        db.collection("users").document(plate).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if(task.isSuccessful()){
                    DocumentSnapshot res = task.getResult();
                    if(res.exists()){
                        Log.d("Debug", "--------"+res.getData());
                        Map<String, Object> data = new HashMap<>(res.getData());
                        if(data.get("plate").equals(plate) && data.get("password").equals(pass)) {
                            Log.d("Debug", "EQUALSSSS");
                            String plate_to_sp = data.get("plate").toString();
                            String phone_to_sp = data.get("phone_number").toString();
                            SharedPreferences.Editor editor = sharedPref.edit();
                            editor.putString("plate", plate_to_sp);
                            editor.putString("phone", phone_to_sp);
                            editor.apply();
                            Intent dialogIntent = new Intent(getApplicationContext(), UserActivity.class);
                            dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            dialogIntent.putExtra("plate", plate_to_sp);
                            startActivity(dialogIntent);
                        }
                        else{
                            Log.d("Debug", "NOTTTT EQUALSSSS");
                        }
                    }
                    else{
                        Log.d("Debug", "------FAILED------");
                    }
                }
            }
        });
    }
    public void search_blockers(String plate)
    {
        Log.d("Debug", "plate "+ plate);
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("blocked").document(plate).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if(task.isSuccessful()){
                    DocumentSnapshot res = task.getResult();
                    if(res.exists()){
                        Map<String, Object> data = new HashMap<>(res.getData());
                        String b_plate = data.get("blocker").toString();
                        String b_phone = data.get("phone").toString();
                        Intent intent = new Intent();
                        intent.setAction("android.intent.action.MAIN");
                        intent.putExtra("blocker_plate", b_plate);
                        intent.putExtra("blocker_phone", b_phone);
                        sendBroadcast(intent);
                    }
                    else{
                        Log.d("debug","failed");
                        Intent intent = new Intent();
                        intent.setAction("android.intent.action.MAIN");
                        intent.putExtra("text", "No Ones Blocking You");
                        sendBroadcast(intent);
                    }
                }
            }
        });
    }
    public void block_user(String plate, Map<String, Object> blocker_data){
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("blocked").document(plate).set(blocker_data);
    }
    public void stop_blocking(String plate){
        final FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("blocked").whereEqualTo("blocker", plate).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if(task.isSuccessful()){
                    for (QueryDocumentSnapshot document : task.getResult()) {
                        String b_plate = document.getId();
                        db.collection("blocked").document(b_plate).delete();
                    }
                }
            }
        });
    }
    public void am_i_blocking(String plate){
        final FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("blocked").whereEqualTo("blocker", plate).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if(task.isSuccessful()){
                    for (QueryDocumentSnapshot document : task.getResult()) {
                        String b_plate = document.getId();
                        Log.d("Debug", "Your blocking " + b_plate);
                        Intent intent = new Intent();
                        intent.setAction("am_i_blocking");
                        intent.putExtra("am_i_blocking", true);
                        intent.putExtra("blocked_plate", b_plate);
                        sendBroadcast(intent);
                    }
                }
            }
        });
    }

}