package com.orito_orc.imblockingyou;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class Blocked extends AppCompatActivity {
    TextView blocking_me;
    Button refresh;
    Button btn_clear;
    BroadcastReceiver br;
    String plate;
    String blocker_plate_temp = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_blocked);
        refresh = (Button)findViewById(R.id.refresh);
        btn_clear = (Button)findViewById(R.id.button_clear);
        btn_clear.setEnabled(false);
        blocking_me = (TextView)findViewById(R.id.blockingMeText);
        plate = getIntent().getStringExtra("plate");
        br = new MyBroadcastReceiver(){
            @Override
            public void onReceive(Context context, Intent intent) {
                String blocker_plate = intent.getStringExtra("blocker_plate");
                String blocker_phone = intent.getStringExtra("blocker_phone");
                String text = intent.getStringExtra("text");
                if(blocker_phone == null || blocker_plate == null){
                    if(text == null){return;}
                    blocking_me.setText(text);
                    return;
                }
                String msg = "You are blocked !\n\nBlockers info\n\nPlate: " + blocker_plate + "\n\nContact number: "+ blocker_phone;
                blocking_me.setText(msg);
                blocker_plate_temp = blocker_plate;
                btn_clear.setEnabled(true);

            }
        };
        IntentFilter intentFilter = new IntentFilter("android.intent.action.MAIN");
        this.registerReceiver(br,intentFilter);
        check_blocking();
        btn_clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), MyFGService.class);
                i.putExtra("flags", 5);
                i.putExtra("plate", blocker_plate_temp);
                startForegroundService(i);
            }
        });
    }

    @Override
    protected void onPause()
    {
        super.onPause();
        try {
            if(br != null){
                unregisterReceiver(br);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    protected void onResume()
    {
        super.onResume();
        IntentFilter intent = new IntentFilter("android.intent.action.MAIN");
        this.registerReceiver(br, intent);
    }

    public void check_blocking()
    {
        Intent i = new Intent(this, MyFGService.class);
        i.putExtra("flags", 4);
        i.putExtra("plate", this.plate);
        startForegroundService(i);
    }
    public void refresh(View v){
        check_blocking();
    }
}
